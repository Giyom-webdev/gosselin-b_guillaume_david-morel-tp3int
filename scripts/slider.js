'use strict'
$(".slider-carousel").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000, //2000ms = 2s;
    autoplayHoverPause: true
})
